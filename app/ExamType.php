<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamType extends Model
{
    protected $hidden = [
        'updated_at',
    ];

    public function exam()
    {
        return $this->hasMany(Exam::class);
    }

    public function examCategory()
    {
        return $this->hasMany(ExamCategory::class);
    }
}
