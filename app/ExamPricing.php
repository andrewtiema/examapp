<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamPricing extends Model
{
    protected $hidden = [
        'updated_at',
    ];

    public function exam()
    {
        return $this->hasMany(Exam::class,'exam_id');
    }
}
