<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamLevel;
use App\ExamPricing;
use App\ExamQuestions;
use App\ExamTopic;
use App\ExamType;
use App\Payment;
use App\Question;
use App\User;
use App\UserExam;
use App\UserExamQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    public function getExams()
    {
        $exams = Exam::with(['examType'])->get();

        $cleanExams = $exams->map(function ($record) {
            return [
                'id' => $record->id,
                'name' => $record->name,
                'description' => $record->description,
                'exam_type' => ExamType::find($record->exam_type_id),
                'average_duration' => $record->average_duration,
                'pricing' => ExamPricing::where(['exam_id' => $record->id])->first(),
                'exam_topic' => ExamTopic::where(['id' => $record->exam_topic_id])->first(),
                'exam_level' => ExamLevel::where(['id' => $record->exam_level_id])->first(),
            ];
        });

        return GeneralResponseHandler::getSingleObjectResponse($cleanExams, 'Exams fetched successfully', count($exams), false);
    }

    public function getExam($id)
    {
        $exam = Exam::where(['id' => $id])->with(['examType', 'pricing'])->first();
        if (!$exam) {
            return GeneralResponseHandler::getErrorResponse(['Resource not found'], true);
        }
        $pricing = ExamPricing::where(['exam_id' => $id])->first();

        // check if user has paid
        $checkForPayment = Payment::where(['user_id' => Auth::user()->id, 'exam_id' => $id])->first();
        if ($checkForPayment) {
            $hasUserPaid = true;
        } else {
            $hasUserPaid = false;
        }
        $responseObject = [
            'exam' => $exam,
            'pricing' => $pricing,
            'hasUserPaid' => $hasUserPaid
        ];

        return GeneralResponseHandler::getSingleObjectResponse($responseObject, 'Exam fetched successfully', 1, false);
    }

    public function examQuestions($id)
    {
        $exam = Exam::where(['id' => $id])->first();
        if (!$exam) {
            return GeneralResponseHandler::getErrorResponse(['Resource not found'], true);
        }

        $userId = Auth::user()->id;
        //check whether user has unanswered questions in user_exam_questions
        $fetchUnanswered = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])
            ->where('answer', '=', null)
            ->get();

        //if not, populate user_exam_questions with questions and return the first one
        if ($fetchUnanswered->isNotEmpty()) {
            //return remaining questions
            $getFirstQuestion = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])
                ->where('answer', '=', null)
                ->first();
            $question = Question::where(['id' => $getFirstQuestion->question_id])->first();
            return GeneralResponseHandler::getSingleObjectResponse($question, 'Exam question fetched successfully', 1, false, 200);
        } else {
            //try fetch existing results
            $fetchExistingQuestions = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])
                ->where('answer', '!=', null)
                ->get();
            if ($fetchExistingQuestions->isNotEmpty()) {
                // return status 600
                $fetchAnsweredQuestions = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])
                    ->where('answer', '!=', null)
                    ->get();

                if (count($fetchAnsweredQuestions) > 0) {
                    $correctCount = count(UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])
                        ->where('isCorrect', '=', '1')
                        ->get());
                    $totalQuestions = count($fetchAnsweredQuestions);
                    $answeredQuestions = collect([]);
                    foreach ($fetchAnsweredQuestions as $fetchAnsweredQuestion) {
                        $answeredQuestion = collect([
                            'id' => $fetchAnsweredQuestion->id,
                            'question' => Question::find($fetchAnsweredQuestion->question_id),
                            'exam' => Exam::find($fetchAnsweredQuestion->exam_id),
                            'answer' => $fetchAnsweredQuestion->answer,
                            'isCorrect' => $fetchAnsweredQuestion->isCorrect
                        ]);
                        $answeredQuestions->push($answeredQuestion);
                    }

                    $responseObject = [
                        'correctQuestions' => $correctCount,
                        'totalQuestions' => $totalQuestions,
                        'questions' => $answeredQuestions
                    ];
                    return GeneralResponseHandler::getSingleObjectResponse($responseObject, 'Finished exam. Your results are as below', 1, false, 600);
                } else {
                    return GeneralResponseHandler::getSingleObjectResponse(null, 'No more exam questions', 1, false, 600);
                }


            } else {
                //fetch all exam questions and populate new table and return first question

                $checkIfExamsQuestionsExist = ExamQuestions::where(['exam_id' => $id])->get();
                if (count($checkIfExamsQuestionsExist) > 0) {
                    $examQuestionsIds = ExamQuestions::where(['exam_id' => $id, 'status' => '1'])->pluck('question_id')->all();
                    foreach ($examQuestionsIds as $questionId) {
                        $userExamQuestion = new UserExamQuestion();
                        $userExamQuestion->user_id = $userId;
                        $userExamQuestion->exam_id = $id;
                        $userExamQuestion->question_id = $questionId;
                        $userExamQuestion->save();
                    }
                    $getFirstQuestion = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])
                        ->where('answer', '=', null)
                        ->first();
                    $question = Question::where(['id' => $getFirstQuestion->question_id])->first();
                    return GeneralResponseHandler::getSingleObjectResponse($question, 'Exam question fetched successfully', 1, false);
                } else {
                    return GeneralResponseHandler::getSingleObjectResponse(null, 'No questions for this exam', 1, false, 450);
                }
            }
        }
    }

    public function examQuestionAnswer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'exam_id' => 'required|exists:exams,id',
            'question_id' => 'required|exists:questions,id',
            'answer' => 'required'
        ]);

        if ($validator->fails()) {
            return GeneralResponseHandler::getErrorResponse($validator->errors()->all(), true);
        }

        $data = $request->json()->all();
        $question = Question::where(['id' => $data['question_id']])->first();
        $answer = $data['answer'];
        $examId = $data['exam_id'];
        $userId = Auth::user()->id;

        $correctAnswer = $question->answers_option['answers']['correct'];

        //check if user has already answered question
        $validateQuestion = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $examId, 'question_id' => $data['question_id']])
            ->where('answer', '!=', null)
            ->first();
        if ($validateQuestion) {
            return GeneralResponseHandler::getErrorResponse(["You have already answered this question"], true);
        }

        //update answer
        $answeredQuestion = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $examId,
            'question_id' => $data['question_id']])
            ->first();
        if ($answeredQuestion) {
            $answeredQuestion->answer = $answer;
            if ($answer == $correctAnswer) {
                $answeredQuestion->isCorrect = true;
            } else {
                $answeredQuestion->isCorrect = false;
            }
            $answeredQuestion->save();

            //fetch next question
            $getNextQuestion = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $examId])
                ->where('answer', '=', null)
                ->first();
            if ($getNextQuestion) {
                $question = Question::where(['id' => $getNextQuestion->question_id])->first();
                return GeneralResponseHandler::getSingleObjectResponse($question, 'Exam question fetched successfully', 1, false, 200);
            } else {
                //return exam is done
                //but before that, fetch how the user has performed in the exam
                $fetchAnsweredQuestions = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $examId])
                    ->where('answer', '!=', null)
                    ->get();

                if (count($fetchAnsweredQuestions) > 0) {
                    $correctCount = count(UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $examId])
                        ->where('isCorrect', '=', '1')
                        ->get());
                    $totalQuestions = count($fetchAnsweredQuestions);
                    $answeredQuestions = collect([]);
                    foreach ($fetchAnsweredQuestions as $fetchAnsweredQuestion) {
                        $answeredQuestion = collect([
                            'id' => $fetchAnsweredQuestion->id,
                            'question' => Question::find($fetchAnsweredQuestion->question_id),
                            'exam' => Exam::find($fetchAnsweredQuestion->exam_id),
                            'answer' => $fetchAnsweredQuestion->answer,
                            'isCorrect' => $fetchAnsweredQuestion->isCorrect
                        ]);
                        $answeredQuestions->push($answeredQuestion);
                    }

                    $responseObject = [
                        'correctQuestions' => $correctCount,
                        'totalQuestions' => $totalQuestions,
                        'questions' => $answeredQuestions
                    ];

                    //update user exam table
                    $percentageScore = floor(($correctCount / $totalQuestions) * 100);

                    $userExam = new UserExam();
                    $userExam->user_id = Auth::user()->id;
                    $userExam->exam_id = $examId;
                    $userExam->score = $percentageScore;
                    //sort this out later
                    $userExam->responses = "";
                    $userExam->save();

                    return GeneralResponseHandler::getSingleObjectResponse($responseObject, 'Finished exam. Your results are as below', 1, false, 600);
                } else {
                    return GeneralResponseHandler::getSingleObjectResponse(null, 'No more exam questions', 1, false, 600);
                }
            }
        } else {
            // throw error
            return GeneralResponseHandler::getErrorResponse(["Sorry an error occurred please try again"], true);
        }
    }

    public function restartExam($id)
    {
        $exam = Exam::where(['id' => $id])->first();
        if (!$exam) {
            return GeneralResponseHandler::getErrorResponse(['Exam not found'], true);
        }
        $userId = Auth::user()->id;

        $validateQuestion = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])->delete();
        if (!$validateQuestion) {
            return GeneralResponseHandler::getErrorResponse(['Sorry an error occurred'], true);
        }

        $examQuestionsIds = ExamQuestions::where(['exam_id' => $id, 'status' => '1'])->pluck('question_id')->all();
        foreach ($examQuestionsIds as $questionId) {
            $userExamQuestion = new UserExamQuestion();
            $userExamQuestion->user_id = $userId;
            $userExamQuestion->exam_id = $id;
            $userExamQuestion->question_id = $questionId;
            $userExamQuestion->save();
        }
        $getFirstQuestion = UserExamQuestion::where(['user_id' => $userId, 'exam_id' => $id])
            ->where('answer', '=', null)
            ->first();
        $question = Question::where(['id' => $getFirstQuestion->question_id])->first();
        return GeneralResponseHandler::getSingleObjectResponse($question, 'Exam question fetched successfully', 1, false);

    }

    public function examHistory()
    {

    }

    public function examTypes()
    {
        $examTypes = ExamType::all();
        return GeneralResponseHandler::getSingleObjectResponse($examTypes, 'Exam types fetched successful', count($examTypes), false);
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users,phone',
            'username' => 'required|unique:users,username',
            'email' => 'required|unique:users,email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return GeneralResponseHandler::getErrorResponse($validator->errors()->all(), true);
        }

        $data = $request->json()->all();

        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->phone = $data['phone'];
        $user->username = $data['username'];
        $user->password = bcrypt($data['password']);
        $user->email = $data['email'];
        $user->save();
        return GeneralResponseHandler::getSingleObjectResponse(null, 'Registration Successful', 1, false);
    }

    //payments
    public function processExamPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'exam_id' => 'required|exists:exams,id',
            'amount' => 'required',
            'transaction_id' => 'required'
        ]);

        if ($validator->fails()) {
            return GeneralResponseHandler::getErrorResponse($validator->errors()->all(), true);
        }

        $data = $request->json()->all();

        $payment = new Payment();
        $payment->user_id = Auth::user()->id;
        $payment->exam_id = $data['exam_id'];
        $payment->amount = $data['amount'];
        $payment->transaction_number = $data['transaction_id'];
        $payment->save();
        return GeneralResponseHandler::getSingleObjectResponse(null, 'Payment made Successfully', 1, false);
    }
}
