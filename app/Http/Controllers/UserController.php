<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamCategory;
use App\ExamType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function loginUser(Request $request)
    {
        $res = ApiController::loginAttempt($request['email'], $request['password']);
        if ($res->status_message == "Success") {
            Session::put('token', $res->data->token);
            Session::put('username', $res->data->username);
            return view('exams.exam')->with('message', 'Login Successfully');
        } else {
            return view('auth.login')->withErrors($res->errors);
        }
    }

    public function createAccount()
    {
        return view('auth.register');
    }

    public function registerUser(Request $request)
    {
        $response = ApiController::registerUser($request);
        if ($response->status_message == "Success") {
            return view('auth.login');
        } else {
            return redirect()->back()->withErrors($response->errors);
        }
    }



    public function getExams()
    {

        $exam_tab_categories = ExamType::with('examCategory')
            ->orderBy('id', 'DESC')
            ->get();

        $exam_categories = ExamCategory::with('exam')
            ->orderBy('id', 'desc')
            ->get();

        $exam_types = ExamType::query()
            ->orderBy('id', 'DESC')
            ->get();

//        $exam_categories_data = Exam::query()
//            ->join('exam_categories', 'exams.exam_category_id', '=', 'exam_categories.id')
//            ->select('exam_categories.id as category_id')
//            ->groupBy('category_id')
//            ->get();

        $exam_categories_data = Exam::with('exam_category')
            ->orderBy('id','desc')
            ->select('exam_category_id')
            ->groupByRaw('exam_category_id')
            ->get();

        $exams = Exam::with('exam_category')
            ->orderBy('id', 'desc')
            ->get();


        return view('welcome', ['exam_categories_data' => $exam_categories_data, 'exams' => $exams, 'exam_categories' => $exam_categories, 'exam_types' => $exam_types, 'exam_tab_categories' => $exam_tab_categories]);
    }



    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function demoQuestions()
    {
        return view('exams.demo-questions');
    }

    public function examQuestions()
    {
        return view('exams.exam-page');
    }
}
