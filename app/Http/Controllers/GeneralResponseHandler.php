<?php
namespace App\Http\Controllers;

class GeneralResponseHandler
{
    public static function getSingleObjectResponse($response, $message, $count, $error, $statusCode = 1){
        return response()->json(
            [
                'status_code' => $statusCode,
                'status_message' => "Success",
                'message' => $message,
                'error' => $error,
                'errors' => null,
                'data' => $response,
                'count' => $count
            ]
        )->setStatusCode(200);

    }


    public static function getErrorResponse($message, $error, $statusCode = 0){
        return response()->json(
            [
                'status_code' => $statusCode,
                'status_message' => "Failed",
                'message' => $message,
                'error' => $error,
                'errors' => $message,
                'data' => null,
                'count' => 0
            ]
        )->setStatusCode(200);

    }

}
