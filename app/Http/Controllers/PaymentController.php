<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Stripe;

class PaymentController extends Controller
{
    public function paymentProcess(Request $request)
    {

        try {
            //set secret
            Stripe::setApiKey(config('services.stripe.secret'));
            $token = $request['stripeToken'];
            $amount = $request['amount'];

            $charge = Charge::create([
                'amount' => floatval($amount) * 100,
                'currency' => 'usd',
                'description' => 'Exam payment',
                'source' => $token,
            ]);
            $exam_id = $request['exam_id'];
            $transaction_id = $charge->id;

            $payment = new Payment();
            $payment->user_id = Auth::user()->id;
            $payment->exam_id = $exam_id;
            $payment->amount = $amount;
            $payment->transaction_number = $transaction_id;
            $payment->save();
            return redirect()->route('exams')->with('success', 'Payment processed successfully');
        } catch (\Stripe\Exception\ApiErrorException $e) {
            return redirect()->route('exams')->with('warning', 'Sorry, something went wrong. Please retry');
        }

    }
}
