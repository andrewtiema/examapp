<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamQuestions;

class ExamController extends Controller
{

    /*
     * todo
     * Clean Up controller
    */

    //Get all exams and display on index page
    public function getAllExams() {
        $exams = Exam::with('exam_type')->get();
        dd($exams);
        return view('welcome',['exams'=>$exams]);
    }


    public function index($id)
    {
        return view('exams.questions', ['id' => $id]);
    }

    public function getExams()
    {
        $exams = Exam::all();

        $cleanExams = $exams->map(function ($record) {
            return [
                'id' => $record->id,
                'name' => $record->name,
                'description' => $record->description,
                'questions' => count(ExamQuestions::where(['exam_id' => $record->id])->get()),
            ];
        });

        return view('exams.exam', ['exams' => $cleanExams]);
    }
}
