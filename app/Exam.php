<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{

    protected $hidden = [
        'updated_at',
    ];

    public function examType()
    {
        return $this->belongsTo(ExamType::class);
    }

    public function exam_category()
    {
        return $this->belongsTo(ExamCategory::class);
    }

    public function pricing()
    {
        return $this->belongsTo(ExamPricing::class);
    }
}
