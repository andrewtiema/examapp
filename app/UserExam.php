<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExam extends Model
{
    protected $casts = [
        'responses' => 'array'
    ];
}
