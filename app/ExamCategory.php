<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamCategory extends Model
{
    public function exam()
    {
        return $this->hasMany(Exam::class);
    }

    public function examType()
    {
        return $this->belongsTo(ExamType::class);
    }
}
