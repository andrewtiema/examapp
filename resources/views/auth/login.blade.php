@extends('layouts.app')
@section('content')
    <div class="section-4">
        <div class="container-2 w-container">
            <h1 class="heading-2">Log In</h1>
            <div class="form-block w-form">
                @include('message.message')
                <form action="{{ route('login') }}" method="post" id="email-form" name="email-form" data-name="Email Form" class="form">
                    @csrf
                    <input type="email" class="form-text-field w-input" autofocus="true" maxlength="256" name="email" data-name="email"  placeholder="Email" id="email" required="">
                    <input type="password" class="form-text-field w-input" maxlength="256" name="password" data-name="Password" placeholder="Password" id="password" required="">
                    <input type="submit" value="Sign In" data-wait="Please wait..." class="standard-button submit-button w-button">
                </form>
            </div>
            <div class="div-block-7"><a href="{{ route('register') }}">Register</a><a href="#">Forgot Password</a></div>
        </div>
    </div>
@endsection
