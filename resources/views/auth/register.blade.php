@extends('layouts.app')
@section('content')
<div class="section-4">
    <div class="container-2 w-container">
        <h1 class="heading-2">Sign Up</h1>
        <div class="form-block w-form">
            @include('message.message')
            <form method="post" action="{{ route('register.post') }}" id="email-form" name="email-form" data-name="Email Form" class="form">
                @csrf
                <input type="text" class="form-text-field w-input" autofocus="true" maxlength="256" name="name" data-name="Name" placeholder="Name" id="name" required="">
                <div class="div-block-11">
                    <input type="email" class="form-text-field left w-input" maxlength="256" name="email" data-name="Email 3" placeholder="Email" id="email" required="">
                    <input type="password" class="form-text-field right w-input" maxlength="256" name="password" data-name="Password" placeholder="Password" id="password" required="">
                </div>
                <input type="submit" value="Sign Up" data-wait="Please wait..." class="standard-button submit-button w-button">
            </form>
        </div>
        <div class="div-block-7"><a href="{{ route('login') }}">Sign in</a><a href="#">Forgot Password</a></div>
    </div>
</div>
@endsection
