@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

@endif
@if(Session::has('message'))
    <div class="alert alert-success fade">{{ Session::get('message') }}</div>
@endif

@if (Session::has('success'))
    <div class="w-form-done">
        <strong>Success!!</strong>
        <p>{{ Session::get('success') }}</p>
    </div>
@endif
@if (Session::has('info'))
    <div class="alert alert-info">
        <strong>Info!</strong>
        <p>{{ Session::get('info') }}</p>
    </div>
@endif
@if (Session::has('warning'))
    <div class="w-form-fail">
        <strong>Warning!</strong>
        <p>{{ Session::get('warning') }}</p>
    </div>
@endif
