<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Aug 13 2020 22:04:26 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5f0ca6d08f9d3e12ad7e4a8e" data-wf-site="5f0ca6d09de0025c20bc22ab">
<head>
    <meta charset="utf-8">
    <title>NCLEX Prep</title>
    <meta content="Guarantee your success when you do the actual NCLEX Exam by Studying and preparing for test day using our Test Prep Resources. Start for FREE." name="description">
    <meta content="NCLEX Prep" property="og:title">
    <meta content="Guarantee your success when you do the actual NCLEX Exam by Studying and preparing for test day using our Test Prep Resources. Start for FREE." property="og:description">
    <meta content="NCLEX Prep" property="twitter:title">
    <meta content="Guarantee your success when you do the actual NCLEX Exam by Studying and preparing for test day using our Test Prep Resources. Start for FREE." property="twitter:description">
    <meta property="og:type" content="website">
    <meta content="summary_large_image" name="twitter:card">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="{{ asset('assets/css/normalize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/webflow.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/antonys-initial-project-b5a7ae.webflow.css')}}" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Nunito:200italic,300,300italic,regular,italic,600,600italic,700,700italic,800,800italic,900,900italic","Fira Sans:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Poppins:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Source Sans Pro:200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,900,900italic","Nunito:200,200italic,300,300italic,regular,italic,600,600italic,700,700italic,800,800italic,900,900italic","Space Mono:regular,italic,700,700italic","Alfa Slab One:regular","DM Sans:regular,italic,500,500italic,700,700italic","DM Serif Display:regular,italic","Quattrocento:regular,700","Fanwood Text:regular,italic"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="{{ asset('assets/images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">
    <link href="{{ asset('assets/images/webclip.png')}}" rel="apple-touch-icon">
</head>
<body class="body">
<div data-w-id="a6c490b2-f012-edaf-63a2-dd9948d95200" style="display:none" class="sign-in-modal">
    <div class="modal-container"><img src="images/icon-cross.svg" alt="" class="modal-close-button">
        <h2 class="modal-header">Log In to Access Locked Tests</h2>
        <div class="div-block-9"><a href="#" class="standard-button w-button">Sign Up</a><a href="#" class="standard-button-alt w-button">Log In</a></div>
    </div>
</div>
<div data-collapse="medium" data-animation="default" data-duration="300" role="banner" class="navbar site-navigation w-nav">
    <div class="navbar-container w-container">
        <a href="{{ route('welcome') }}" aria-current="page" class="brand-link w-nav-brand w--current">
            <h3 id="site-title" class="site-title">NCLEX Prep</h3>
        </a>
        @if(\Illuminate\Support\Facades\Auth::user())
            <nav role="navigation" class="nav-menu w-nav-menu"><a href="#" id="nav-link" class="nav-link w-nav-link">Pricing</a><a href="{{ route('demo.questions') }}" id="nav-link" class="nav-link w-nav-link">Demo Questions</a><a href="{{ route('logout') }}" id="nav-link" class="nav-link w-nav-link">Logout</a></nav>
        @else
        <nav role="navigation" class="nav-menu w-nav-menu"><a href="#" id="nav-link" class="nav-link w-nav-link">Pricing</a><a href="{{ route('demo.questions') }}" id="nav-link" class="nav-link w-nav-link">Demo Questions</a><a href="{{ route('login') }}" id="nav-link" class="nav-link w-nav-link">Login</a><a href="{{ route('register') }}" class="nav-link w-nav-link">Register</a></nav>
        @endif
        <div class="menu-button w-nav-button">
            <div class="menu-button-icon w-icon-nav-menu"></div>
        </div>
    </div>
</div>

@yield('content')

<footer id="footer" class="footer">
    <div class="w-container">
        <div class="footer-flex-container">
            <div class="div-block-10">
                <ul role="list" class="list w-list-unstyled">
                    <li><a href="#" class="footer-link">Home</a></li>
                    <li><a href="#" class="footer-link">Pricing</a></li>
                    <li><a href="#" class="footer-link">Practice Questions</a></li>
                    <li><a href="#" class="footer-link">Log In</a></li>
                    <li><a href="#" class="footer-link">Sign Up</a></li>
                </ul>
            </div>
            <div>
                <ul role="list" class="list-2 w-list-unstyled">
                    <li><a href="#" class="footer-link">Twitter</a></li>
                    <li><a href="#" class="footer-link">Instagram</a></li>
                </ul>
            </div>
        </div>
        <div class="text-block-3">Copyright © 2020. All rights reserved. | Ux by <a href="https://halisihub.com" target="_blank">halisiHub</a></div>
    </div>
</footer>
<script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js?site=5f0ca6d09de0025c20bc22ab" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="{{ asset('assets/js/webflow.js')}}" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>
