@extends('layouts.app')
@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('assets/images/banner/quiz.jpg') }}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1>Quiz</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb white-bg">
        <div class="container">
            <ul>
                <li><a href="{{route('exams')}}">Exams</a></li>
                <li><a href="#">Quiz</a></li>
            </ul>
        </div>
    </section>
    <section class="quiz-view">
        <div class="container">
            <div class="quiz-title">
                <h2>General Quiz</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-2">
                </div>
                <div class="col-sm-8 col-md-12">
                    <div class="qustion-main">
                        <div class="qustion-box">
                            <div id="noResults" class="hidden">
                                <h3>Sorry We Dont Have A Questions For This Test</h3>
                                <br>
                                <a class="btn" href="{{ route('exams') }}">Back</a>
                            </div>

                            <!--DISPLAY LOADERS-->
                            <div class="loader" id="examLoader"></div>
                            <div id="doneSubmit"></div>
                            <!--END DISPLAY LOADERS-->

                            <!--DISPAY EXAM CONTENT-->
                            <div>
                                <h4 id="questionContent"></h4>
                            </div>
                            <div id="questionImage" class="hidden"></div>
                            <!--END DISPAY EXAM CONTENT-->

                            <!--Done Questions Display-->
                            <div id="questionsDone" class="hidden">
                                @auth()
                                    <h5>HELLO {{ \Illuminate\Support\Facades\Auth::user()->first_name }} YOU HAVE
                                        ALREADY
                                        DONE THIS EXAM WOULD YOU LIKE TO RESTART?</h5>
                                @endauth
                            </div>

                            <!--DISPLAY EXAM ANSWERS-->
                            <div class="ans">
                                <div class="ans-slide">
                                    <div id="optionAnswers">
                                    </div>
                                </div>
                            </div>
                            <!--END DISPLAY EXAM ANSWERS-->

                            <!--DISPLAY EXAM RESULTS -->
                            <div id="results" class="hidden">
                                <h3 id="resultHeader" class="hidden">You have a total of: <span
                                        id="correctAnswers"></span>/<span
                                        id="totalQuestions"></span></h3>
                                <br>
                                <div id="questionAnswerResults" class="hidden">

                                </div>
                            </div>
                            <!--DISPLAY EXAM RESULTS END-->
                            <div class="btn-slide">
                                <input type="hidden" id="hiddenExamId">
                                <input type="hidden" id="hiddenQuestionId">
                                <a href="javascript:;" onclick="submitQuestion()" id="nextButton" class="btn hidden"><i
                                        class="fa fa-greater-than">Next</i></a>
                            </div>
                        </div>
                        <div id="restartExam" class="hidden">
                            <p>Do you want to restart the exam?</p>
                            <a href="javascript:;" onclick="restartExam()" class="btn">Yes</a>
                            <a href="{{ route('exams') }}" class="btn">No</a>
                        </div>
                        <div class="submit-quiz">
                            <a id="buttonSubmit" class="btn hidden">View Results</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script>
        $(document).ready(function () {
            fetchExamQuestions();
        });

        function fetchExamQuestions() {
            let request = $.ajax({
                url: '/exam-questions/{{ $id }}',
                method: "GET",
                data: {},
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            request.done(function (response) {
                //Check for successful response
                if (response.status_code === 200) {
                    $('#questionsDone').addClass('hidden');
                    $('#resultHeader').addClass('hidden');
                    $('#questionAnswerResults').addClass('hidden');
                    $('#examLoader').addClass('hidden');
                    $('#nextButton').removeClass('hidden');
                    bind(response.data)
                    console.log(response);
                }
                //Check if user has done exams
                else if (response.status_code === 600) {
                    console.log('Done')
                    $('#examLoader').addClass('hidden');
                    $('#questionContent').removeClass('hidden');
                    bindResults(response.data)
                    $('#restartExam').removeClass('hidden');
                }
                //check if there is no test available
                else if (response.status_code === 450) {
                    $('#resultHeader').addClass('hidden');
                    $('#examLoader').addClass('hidden');
                    $('#questionsDone').addClass('hidden');
                    $('#noResults').removeClass('hidden');
                    $('#questionContent').addClass('hidden');
                    $('#optionAnswers').addClass('hidden');
                } else {
                    $('#examLoader').addClass('hidden');
                    swal({
                            title: "Sorry an error occured",
                            text: response.errors,
                            type: "warning",
                            // showCancelButton: true,
                            closeOnConfirm: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Go To Home",
                        },
                        function () {
                            window.location.href = "{{URL::to('exams')}}"
                        });
                }

            });

            request.fail(function (jqXHR, textStatus) {
                $('#examLoader').addClass('hidden');
                if (jqXHR.status === 401) {
                    swal({
                            title: "Sorry you are unauthenticated",
                            text: "You have to login to proceed",
                            type: "warning",
                            // showCancelButton: true,
                            closeOnConfirm: false,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Go To Login",
                        },
                        function () {
                            window.location.href = "{{URL::to('login')}}"
                        });
                }

            });
        }

        function bind(data) {
            $('#questionContent').removeClass('hidden');
            $('#questionContent').text(data.title);
            $('#hiddenQuestionId').text(data.id);
            $('#optionAnswers').empty();
            $('#questionImage').empty();
            if (data.question_image == null) {
                $('#questionImage').addClass('hidden');
            } else {
                $('#questionImage').removeClass('hidden');
                var questionImage = '<img height="300px" src="' + data.question_image + '" />';
                $('#questionImage').append(questionImage);
            }
            var filteredArray = data.answers_option.answers;
            delete filteredArray['correct'];

            console.log(filteredArray);
            $.each(filteredArray, function (key, value) {
                $('#optionAnswers').removeClass('hidden');
                var radioHTML = '<label class="containerBox"><input id="answer" type="radio" name="radio" value="' + value + '"><span class="checkmark"></span> ' + value + '</label><br>';
                $('#optionAnswers').append(radioHTML);
            });
        }

        function submitQuestion() {
            $('#optionAnswers').addClass('hidden');
            $('#nextButton').addClass('hidden');
            $('#questionContent').addClass('hidden');
            $('#questionImage').addClass('hidden');
            $('#examLoader').removeClass('hidden');
            let questionId = $('#hiddenQuestionId').text();
            let examAnswer = $('#answer:checked').val();
            $.ajax({
                url: '/exam-question-answer',
                type: 'POST',
                async: false,
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: JSON.stringify({
                    exam_id: {{ $id }},
                    question_id: questionId,
                    answer: examAnswer
                }),
                processData: false,
                success: function (response) {
                    $('#nextButton').removeClass('hidden');
                    $('#examLoader').addClass('hidden');
                    console.log(response);
                    if (response.status_code === 200) {
                        console.log("Next exam" + response);
                        bind(response.data);
                    } else if (response.status_code === 600) {
                        console.log('You have finished exams Hureee');
                        $('#nextButton').addClass('hidden');
                        $('#examLoader').addClass('hidden');
                        var questionsComplete = 'YOU HAVE ANSWERED ALL QUESTIONS IN THIS TEST'
                        $('#doneSubmit').append(questionsComplete);
                        $('#buttonSubmit').removeClass('hidden');
                        $('#buttonSubmit').click(bindResults(response.data));
                    } else {
                        swal({
                                title: "Sorry an error occured",
                                text: response.errors,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: false
                            },
                            function () {
                                location.reload();
                            });
                    }
                },
            });
        }

        function bindResults(results) {
            $('#restartExam').removeClass('hidden');
            $('#questionAnswerResults').empty();
            $('#buttonSubmit').addClass('hidden');
            $('#resultHeader').removeClass('hidden');
            $('#results').removeClass('hidden');
            $('#correctAnswers').text(results.correctQuestions);
            $('#totalQuestions').text(results.totalQuestions);
            $('#questionAnswer').text();
            $('#questionAnswerResults').removeClass('hidden');
            $.each(results.questions, function (index, value) {
                console.log(value);
                var resultsHtml = '<div class="row"><div class="col-lg-12 table-bordered"><br><h3>' + value.question.title + '</h3><br>' +
                    '<p><b>Reasoning:</b> ' + value.question.answer_description + '</p>' +
                    '<p><b>Your Answer:</b> ' + value.answer + '</p>' +
                    '<p><b>Correct Answer:</b> ' + value.question.answers_option.answers.correct + '</p><br></div></div>'
                console.log(value.question.answers_option.answers.correct);
                $('#questionAnswerResults').append(resultsHtml);
            });
        }
    </script>

    <script>
        function restartExam() {
            $('#results').addClass('hidden');
            $('#optionAnswers').addClass('hidden');
            $('#nextButton').addClass('hidden');
            $('#questionContent').addClass('hidden');
            $('#examLoader').removeClass('hidden');
            $.ajax({
                url: '/restart-exam/{{ $id }}',
                type: 'POST',
                async: false,
                dataType: 'json',
                contentType: 'application/json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                success: function (response) {
                    $('#examLoader').addClass('hidden');
                    $('#nextButton').removeClass('hidden');
                    $('#restartExam').addClass('hidden');
                    $('#doneSubmit').addClass('hidden');
                    console.log(response);
                    if (response.status_message === 'Success') {
                        console.log("Next exam" + response);
                        bind(response.data);
                    } else {
                        swal({
                                title: "Sorry an error occured",
                                text: response.errors,
                                type: "warning",
                                // showCancelButton: true,
                                closeOnConfirm: true
                            },
                            function () {
                                //swal("!", "", "success");
                            });
                        console.log(response.errors);
                    }
                },
            });
        }

    </script>


@endsection


