@extends('layouts.app')
@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{asset('assets/images/banner/quiz.jpg')}}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1>Quiz</h1>
            </div>
        </div>
    </section>
    <section class="quiz-view">
        <div class="container">
            <div class="quiz-title">
                <h2>General Quiz</h2>
                <p>Are You Ready For The Test? </p>
            </div>
            @include('message.message')
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div class="time-info"><h3><span style="color: #8b8b8b">Available Tests</span></h3></div>
                    <div id="countdown_stop"></div>
                    <div class="qustion-list">
                        @foreach($exams as $exam)
                            <div class="qustion-slide" id="{{$exam['id']}}" onclick="getSingleExam(this.id)">
                                <div class="qustion-number">{{$exam['name']}}</div>
                                <span>{{$exam['questions']}}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="quiz-intro">
                        <h3 id="examTitleIntro"></h3>
                        <h3 id="getExamStarted">Select Test To Get Started Start</h3>
                        <p id="examDescription"></p>
                        <div class="start-btn" id="examButton"></div>

                        <div id="examAmountCard" class="hidden">This Exam Costs $<span id="examAmount"></span>
                            <p>You can pay via card below</p>
                        </div>
                        <div id="payment" class="payment hidden">
                            <form action="{{ route('payment') }}" method="get">
                                @csrf
                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="{{ config('services.stripe.key') }}"
                                    data-name="Exam Charges"
                                    data-description="Pay For Your Exam To Get Started"
                                >
                                </script>
                                <input type="hidden" id="exam_id" name="exam_id">
                                <input type="hidden" id="amount" name="amount">
                            </form>
                        </div>
                        <div id="paymentIsFree" class="hidden">
                            <h3> Free</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#examButton').hide();
            $('#amount').hide();
            $('#examDescription').hide();
            $('#examTitleIntro').hide();
            $('#dataLoader').show();
            $('#course').hide();
        });

        function getSingleExam(id) {
            $('.qustion-slide').removeClass('active');
            $('#' + id).addClass('active');
            $.ajax({
                type: 'GET',
                url: 'exam/' + id,
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    if (result.status_message === "Success") {
                        $('#getExamStarted').hide();
                        $('#examDescription').show();
                        $('#examButton').show();
                        bindSingleExam(result.data)
                    }
                },
                error: function (result) {
                    console.log("Error" + result.errors);
                }
            });

            function bindSingleExam(data) {
                //display exam
                $('#examAmount').empty();

                $('#examButton').empty();
                $('#examTitleIntro').show();
                $('#examTitleIntro').text(data.exam.name);
                $('#examDescription').text(data.exam.description || "No description");
                var exam_id = data.exam.id;
                $('#examButton').append('<a id="examButtonLink" href="/enroll-exam/' + exam_id + '"  class="btn">Start Quizz</a>');
                $('#exam_id').val(data.exam.id);

                //Check if exam is free
                if (data.pricing != null) {
                    if (parseInt(data.pricing.amount) > 0) {
                        if (data.hasUserPaid === false) {
                            $('#examButtonLink').attr("data-toggle", "tooltip");
                            $('#examButtonLink').attr("title", "Please pay first to access this exam");
                            $('#examButtonLink').attr("data-placement", "top")
                            $("[data-toggle = 'tooltip']").tooltip();
                            $("#examButtonLink").attr('href', '');

                            $('#examAmountCard').removeClass('hidden');
                            $('#payment').removeClass('hidden');
                        } else {
                            $('#examAmountCard').addClass('hidden');
                            $('#payment').addClass('hidden');
                            $('#examButtonLink').removeClass('disabled');
                        }

                        $('#paymentIsFree').addClass('hidden');
                        $('#amount').val(data.pricing.amount);
                        $('#examAmount').text(data.pricing.amount);
                    }
                } else {
                    $('#examAmountCard').addClass('hidden');
                    $('#paymentIsFree').removeClass('hidden');
                    $('#payment').addClass('hidden');
                }
            }

        }
    </script>

@endsection
