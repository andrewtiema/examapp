@extends('layouts.app')
@section('content')
    <div class="section-7">
        <div class="w-container">
            <h1 class="question-page-title">Heading</h1>
        </div>
    </div>
    <div class="section-8">
        <div class="container-3 w-container">
            <div class="w-layout-grid grid-2">
                <div id="w-node-470971087bdf-f1576344">
                    <div class="w-layout-grid question-number-grid">
                        <a href="#" class="question-number-grid-item done-question-incorrect w-inline-block">
                            <div class="question-number-grid-item-number">1</div>
                        </a>
                        <a href="#" class="question-number-grid-item done-question-correct w-inline-block">
                            <div class="question-number-grid-item-number">2</div>
                        </a>
                        <a href="#" class="question-number-grid-item current-question w-inline-block">
                            <div class="question-number-grid-item-number">3</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">4</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">5</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">6</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">7</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">8</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">9</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">10</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">11</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">12</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">12</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">14</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">15</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">16</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">17</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">18</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">19</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">20</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">21</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">22</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">23</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">24</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">25</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">26</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">27</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">28</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">29</div>
                        </a>
                        <a href="#" class="question-number-grid-item w-inline-block">
                            <div class="question-number-grid-item-number">30</div>
                        </a>
                    </div>
                    <div class="div-block-8">
                        <div class="question-page-button"><img src="{{ asset('assets/images/Icon-awesome-chevron-right.svg') }}" width="7" height="12" alt="" class="image">
                            <div>All Tests</div>
                        </div>
                        <div class="question-page-button"><img src="{{ asset('assets/images/Icon-awesome-chevron-right.svg')}}" width="7" height="12" alt="" class="image">
                            <div>Restart</div>
                        </div>
                    </div>
                </div>
                <div id="w-node-ab46ea8f8dae-f1576344">
                    <div>A client hospitalized with severe depression and suicidal ideation refuses to talk with the nurse. The nurse recognizes that the suicidal client has difficulty:</div>
                    <div>
                        <div class="question-field">
                            <div class="question-field-checkbox"></div>
                            <div class="question-field-text">This is some text inside of a div block.</div>
                        </div>
                        <div class="question-field question-incorrect">
                            <div class="question-field-checkbox question-incorrect"></div>
                            <div class="question-field-text">This is some text inside of a div block.</div>
                        </div>
                        <div class="question-field">
                            <div class="question-field-checkbox"></div>
                            <div class="question-field-text">This is some text inside of a div block.</div>
                        </div>
                        <div class="question-field">
                            <div class="question-field-checkbox"></div>
                            <div class="question-field-text">This is some text inside of a div block.</div>
                        </div>
                        <div class="question-field">
                            <div class="question-field-checkbox"></div>
                            <div class="question-field-text">This is some text inside of a div block.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
