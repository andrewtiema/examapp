@extends('layouts.app')
@section('content')
    <div class="section-5">
        <div class="w-container">
            <h1 class="page-title dark-background">Practice Tests</h1>
        </div>
    </div>
    <div class="section-6">
        <div class="w-container">
            <div data-duration-in="300" data-duration-out="100" class="exam-cards-tab-main w-tabs">
                <div class="tabs-menu w-tab-menu">
                    <a data-w-tab="Tab 1" class="tab-link-box w-inline-block w-tab-link w--current">
                        <div class="tab-link-text">NCLEX RN</div>
                    </a>
                    <a data-w-tab="Tab 2" class="tab-link-box w-inline-block w-tab-link">
                        <div class="tab-link-text">NCLEX PN</div>
                    </a>
                </div>
                <div class="tabs-content w-tab-content">
                    <div data-w-tab="Tab 1" class="w-tab-pane w--tab-active">
                        <div data-duration-in="300" data-duration-out="100" class="nclex-pn-tab-content w-tabs">
                            <div class="tabs-menu-2 w-tab-menu">
                                <a data-w-tab="Tab 1" class="inner-tab-title-box w-inline-block w-tab-link w--current">
                                    <div>Psychological</div>
                                </a>
                                <a data-w-tab="Tab 2" class="inner-tab-title-box w-inline-block w-tab-link">
                                    <div>Safe &amp; Effective Care</div>
                                </a>
                                <a data-w-tab="Tab 3" class="inner-tab-title-box w-inline-block w-tab-link">
                                    <div>Health Promotion</div>
                                </a>
                                <a data-w-tab="Tab 4" class="inner-tab-title-box w-inline-block w-tab-link">
                                    <div>SATA</div>
                                </a>
                            </div>
                            <div class="w-tab-content">
                                <div data-w-tab="Tab 1" class="inner-tab-pane w-tab-pane w--tab-active">
                                    <div class="w-layout-grid exam-card-parent-grid">
                                        <div class="exam-card-unlocked">
                                            <div class="exam-card-title">Psychological Integrity 1</div>
                                            <div class="exam-card-title question-numbers">30 QUestions</div>
                                        </div>
                                        <div class="exam-card-unlocked">
                                            <div class="exam-card-title">Psychological Integrity 1</div>
                                            <div class="exam-card-title question-numbers">30 QUestions</div>
                                        </div>
                                        <div class="exam-card-unlocked">
                                            <div class="exam-card-title">Psychological Integrity 1</div>
                                            <div class="exam-card-title question-numbers">30 QUestions</div>
                                        </div>
                                        <div data-w-id="15f07043-74c1-7b29-7312-97d3e39ec882" class="exam-card-locked">
                                            <div class="exam-card-title">Psychological Integrity 1</div>
                                            <div class="exam-card-title question-numbers">30 QUestions</div>
                                        </div>
                                    </div>
                                </div>
                                <div data-w-tab="Tab 2" class="inner-tab-pane w-tab-pane"></div>
                                <div data-w-tab="Tab 3" class="inner-tab-pane w-tab-pane"></div>
                                <div data-w-tab="Tab 4" class="inner-tab-pane w-tab-pane"></div>
                            </div>
                        </div>
                    </div>
                    <div data-w-tab="Tab 2" class="w-tab-pane">
                        <div data-duration-in="300" data-duration-out="100" class="nclex-pn-tab-content w-tabs">
                            <div class="tabs-menu-2 w-tab-menu">
                                <a data-w-tab="Tab 1" class="inner-tab-title-box w-inline-block w-tab-link">
                                    <div>Psychological</div>
                                </a>
                                <a data-w-tab="Tab 2" class="inner-tab-title-box w-inline-block w-tab-link w--current">
                                    <div>Safe &amp; Effective Care</div>
                                </a>
                                <a data-w-tab="Tab 3" class="inner-tab-title-box w-inline-block w-tab-link">
                                    <div>Health Promotion</div>
                                </a>
                                <a data-w-tab="Tab 4" class="inner-tab-title-box w-inline-block w-tab-link">
                                    <div>SATA</div>
                                </a>
                            </div>
                            <div class="w-tab-content">
                                <div data-w-tab="Tab 1" class="inner-tab-pane w-tab-pane"></div>
                                <div data-w-tab="Tab 2" class="inner-tab-pane w-tab-pane w--tab-active"></div>
                                <div data-w-tab="Tab 3" class="inner-tab-pane w-tab-pane"></div>
                                <div data-w-tab="Tab 4" class="inner-tab-pane w-tab-pane"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
