@extends('layouts.app')
@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('assets/images/banner/courses-banner.jpg')}}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1>My Courses</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="my-courses.html">My Courses</a></li>
            </ul>
        </div>
    </section>
    <div class="user-dashboard">
        <div class="container">
            <div class="section-title">
                <h2>My Courses</h2>
            </div>
            <div class="archived-course">
                <div class="course-list last">
                    <div class="img">
                        <img src="{{ asset('assets/images/courses/courses-img4.jpg')}}" alt="">
                    </div>
                    <div class="info">
                        <div class="name"></div>
                        <div class="date">Ended - Jul 08, 2016 </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <div class="share-course">
                            <label>Share Course</label>
                            <ul>
                                <li><a href="my-courses.html#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="my-courses.html#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="my-courses.html#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="btn-block">
                            <a href="my-courses.html#" class="unenroll-link">Unenroll</a>
                            <a href="course-lessons.html" class="btn">Go to classroom</a>
                            <a href="course-progress.html" class="btn">Course progress</a>
                            <a href="course-home.html" class="btn">Course home</a>
                        </div>
                    </div>
                </div>
                <div class="pagination">
                    <ul>
                        <li class="next"><a href="my-courses.html#"><i class="fa fa-angle-left"></i><span>Next</span></a></li>
                        <li class="active"><a href="my-courses.html#">1</a></li>
                        <li><a href="my-courses.html#">2</a></li>
                        <li><a href="my-courses.html#">3</a></li>
                        <li><a href="my-courses.html#">4</a></li>
                        <li class="prev"><a href="my-courses.html#"><span>prev</span> <i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
