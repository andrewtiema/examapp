@extends('layouts.app')
@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('assets/images/banner/quiz.jpg')}}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1>Quiz Result</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb white-bg">
        <div class="container">
            <ul>
                <li><a href="quiz-result.html#">Home</a></li>
                <li><a href="quiz-result.html#">Quiz</a></li>
            </ul>
        </div>
    </section>
    <section class="quiz-view">
        <div class="container">
            <div class="quiz-title">
                <h2>Quiz</h2>
                <p></p>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div id="countdown_stop"></div>
                    <div class="qustion-list">
                        @foreach($exam->questions as $question)
                            <div class="qustion-slide">
                                <div class="qustion-number">{{ $question->question->title }}</div>
                                <span>2</span>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="quiz-result">
                        <h3>Stats</h3>
                        <div class="result-info">
                            <div class="info-slide">
                                <p>Average<span>{{ $exam->totalQuestions }}</span></p>
                            </div>
                            <div class="info-slide">
                                <p>Correct<span>{{ $exam->correctQuestions }}</span></p>
                            </div>
                            <div class="info-slide">
                                <p>Min<span>0</span></p>
                            </div>
                        </div>
                        <div class="leaderboard">
                            <h3>leaderboard</h3>
                            <div class="qustion-review">
                                <p>Lorem ipsum dolor sit amet,<span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Nunc malesuada dui id ex tristique<span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Sed rhoncus nunc id risus sollicitudin, <span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Morbi lobortis leo vitae justo tincidunt<span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Aliquam eu justo eget felis<span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Etiam non lacus consectetur<span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Pellentesque sagittis mi eu<span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Maecenas egestas turpis <span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Mauris aliquet dui non <span>0</span></p>
                            </div>
                            <div class="qustion-review">
                                <p>Aenean ultricies arcu<span>0</span></p>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
