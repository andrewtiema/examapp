@extends('layouts.app')
@section('content')

    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('/assets/images/banner/aboutUs-banner.jpg') }}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1>About Us</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="about.blade.php">About Us</a></li>
            </ul>
        </div>
    </section>
    <section class="about-page">
        <section class="about-ourInfo">
            <div class="container">
                <div class="section-title">
                    <h2>About Academy</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Background</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>

                    </div>
                    <div class="col-sm-6">
                        <h3>Vision and Mission</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-students">
            <div class="container">
                <div class="section-title">
                    <h2>Why students love Academy ?</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="box">
                            <i class="fa fa-trophy"></i>
                            <h4>Courses lead to recruitment</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page  </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="box">
                            <i class="fa fa-pencil"></i>
                            <h4>Creative learning</h4>
                            <p>when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="box">
                            <i class="fa fa-book"></i>
                            <h4>Learn with experts</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page  </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="box">
                            <i class="fa fa-graduation-cap"></i>
                            <h4>On demand</h4>
                            <p>when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal </p>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section class="about-target-audience">
            <div class="container">
                <div class="section-title">
                    <h2>Our target audience</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, </p>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, </p>
                    </div>
                    <div class="col-sm-6 text-center">
                        <img src="{{ asset('/assets/images/about-img1.png')}}" alt="">
                    </div>
                </div>
            </div>
        </section>
        <section class="about-goals">
            <div class="container">
                <div class="section-title">
                    <h2>Goals and objectives of Academy venture</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ asset('/assets/images/about-img2.png')}}" alt="">
                    </div>
                    <div class="col-sm-6">
                        <ul class="goals-point">
                            <li>There are many variations of passages of Lorem Ipsum available many variations of passages of Lorem Ipsum available</li>
                            <li>If you are going to use a passage of Lorem Ipsum, you</li>
                            <li>All the Lorem Ipsum generators on the Internet many variations of passages of Lorem Ipsum available</li>
                            <li>There are many variations of passages of Lorem Ipsum available</li>
                            <li>If you are going to use a passage of Lorem Ipsum, you many variations of passages of Lorem Ipsum available</li>
                            <li>All the Lorem Ipsum generators on the Internet</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

    </section>

@endsection
