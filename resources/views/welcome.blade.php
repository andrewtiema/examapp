@extends('layouts.app')
@section('content')

    <div class="section">
        <div class="w-container">
            <div class="div-block-3">
                <h1 class="page-title">Prepare For Your Exam with Ease</h1>
                <div class="subtitle">Guarantee your success when you do the actual NCLEX Exam by Studying and preparing
                    for test day using our Test Prep Resources. You Can even start for FREE.
                </div>
                <a href="#practice-questions" class="standard-button w-button">Start Free Test</a></div>
        </div>
    </div>
    <section class="section-2">
        <div class="w-container">
            <div class="cta-background-image nurse-phone-bg">
                <div class="overlay-brown">
                    <div class="div-block-4">
                        <div class="text-block-2">This is sLife can be busy and getting time to study can be hectic. The
                            great news is that you can study on the go wit NCLEX Exam&#x27;s mobile friendly test prep
                            platform.ome text inside of a div block.
                        </div>
                        <div class="text-block-2 bold-body-text">Register for Free &amp; Start Preping on the go.</div>
                        <a href="#" class="standard-button smaller w-button">View Practice Questions</a></div>
                </div>
            </div>
        </div>
    </section>

    {{--  Exam Section  --}}
    <section id="practice-questions" class="section-3">
        <div class="w-container">
            <h1 class="subheading">Start Your NCLEX Practice Test</h1>
            <div class="div-block-6"></div>
            <div data-duration-in="300" data-duration-out="100" class="exam-cards-tab-main w-tabs">
                <div class="tabs-menu w-tab-menu">
                    @foreach($exam_types as $exam_type)
                        <a data-w-tab="Tab {{ $exam_type->id }}"
                           class="tab-link-box w-inline-block w-tab-link <?php if (\App\ExamType::first()->id == $exam_type->id) {
                               echo "w--current";
                           }?>">
                            <div class="tab-link-text">{{ $exam_type->name }}</div>
                        </a>
                    @endforeach

                </div>

                <div class="tabs-content w-tab-content">

                    @foreach($exam_tab_categories as $tab_category)
                        <div data-w-tab="Tab {{ $tab_category->id }}"
                             class="w-tab-pane <?php if (\App\ExamType::first()->id == $tab_category->id) {
                                 echo "w--tab-active";
                             }?> ">

                            <div data-duration-in="300" data-duration-out="100" class="nclex-pn-tab-content w-tabs">

                                <div class="tabs-menu-2 w-tab-menu">
                                    @foreach($exam_categories as $exam_category)
                                        <a data-w-tab="Tab {{ $exam_category->id }}"
                                           href="javascript:;"
                                           onclick="window.location.href='{{ route('welcome',['id'=>$exam_category->id]) }}'"
                                           class="inner-tab-title-box w-inline-block w-tab-link <?php if ($exam_category->id == isset($_REQUEST['id'])) {
                                               echo "w--current";
                                           }?> ">
                                            <div>{{ $exam_category->name }}</div>
                                        </a>
                                    @endforeach

                                </div>


                                <div class="w-tab-content">
                                    @foreach($exam_categories_data as $exam_category_tab)
                                        <div data-w-tab="Tab <?php if (isset($_REQUEST['id'])) {
                                            $id = $_REQUEST['id'];
                                            echo $id;
                                        } else {
                                            echo 0;
                                        }
                                        ?>"class="inner-tab-pane w-tab-pane
                                                <?php if (isset($_REQUEST['id']) == $exam_category_tab->exam_category_id) {
                                                 echo "w--tab-active";
                                             }
                                             ?>">
                                                <div class="w-layout-grid exam-card-parent-grid">
                                                    @foreach(\App\Exam::where(['exam_category_id'=>isset($_REQUEST['id'])])->get() as $exam)
                                                        <a href="{{ $exam->id }}">
                                                            <div class="exam-card-unlocked">
                                                                <div class="exam-card-title">{{ $exam->name }}</div>
                                                                <div class="exam-card-title question-numbers">30 QUestions</div>
                                                            </div>
                                                        </a>
                                                    @endforeach
                                                </div>

                                        </div>
                                    @endforeach

                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </section>
@endsection
