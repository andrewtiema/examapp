<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'welcome','uses'=>'UserController@getExams']);

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('exam-questions/{id}', 'ApiController@examQuestions');
    Route::post('exam-question-answer','ApiController@examQuestionAnswer');
    Route::get('exam-history', 'ApiController@examHistory');
    Route::get('exam-types', 'ApiController@examTypes');
    Route::post('restart-exam/{id}', 'ApiController@restartExam');
    Route::get('exam-payment', 'ApiController@processExamPayment');

    Route::get('/exams',['as'=>'exams','uses'=>'ExamController@getExams']);

    Route::get('get-exams', 'ApiController@getExams');

    Route::get('exam/{id}', [
        'as' => 'exam', 'uses' => 'ApiController@getExam'
    ]);

    Route::get('/enroll-exam/{id}',['uses'=>'ExamController@index','as'=>'enroll.exam']);

    Route::get('/payment',['uses'=>'PaymentController@paymentProcess','as'=>'payment']);

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/logout',['as'=>'logout-user','uses'=>'UserController@logout']);
});

Route::get('/login', [
    'as' => 'login', 'uses' => 'Auth\LoginController@loadLogin'
]);

Route::post('/login-user',['as'=>'login-user','uses'=>'UserController@loginUser']);
Route::post('/signUp',['as'=>'register-user','uses'=>'UserController@registerUser']);
Route::get('/register',  [
    'as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm'
]);

Route::post('/register-post',  [
    'as' => 'register.post', 'uses' => 'Auth\RegisterController@registerUser'
]);


Route::get('/create-account',['as'=>'create.account','uses'=>'Auth\RegisterController@showRegistrationForm']);
//Route::get('/create-account',['as'=>'create.account','uses'=>'UserController@createAccount']);

Route::get('/demo-questions',['as'=>'demo.questions', 'uses'=>'UserController@demoQuestions']);
Route::get('/exam-questions',['as'=>'exam.questions', 'uses'=>'UserController@examQuestions']);


