<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLevelToExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exams', function (Blueprint $table) {
            $table->bigInteger('exam_topic_id')->unsigned()->nullable()->after('average_duration');
            $table->foreign('exam_topic_id')->references('id')->on('exam_topics');
            $table->bigInteger('exam_level_id')->unsigned()->nullable();
            $table->foreign('exam_level_id')->references('id')->on('exam_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exams', function (Blueprint $table) {
            //
        });
    }
}
