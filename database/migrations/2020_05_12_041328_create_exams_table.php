<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('exam_type_id')->unsigned();
            $table->foreign('exam_type_id')->references('id')->on('exam_types');
            $table->bigInteger('exam_category_id')->unsigned();
            $table->foreign('exam_category_id')->references('id')->on('exam_categories');
            $table->string('difficulty_level');
            $table->string('average_duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
